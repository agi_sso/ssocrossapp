#
# Be sure to run `pod lib lint CrossAppLogin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CrossAppLogin'
  s.version          = '1.0.8'
  s.summary          = 'This pod support cross app login i.e. single sign on.'


  s.description      = 'This pod is for iOS Apps of Times Internet Limited which sign in is provided and handled by AGI SSO Team. Apps belonging to the same team support cross app login i.e. login in one app will do login in all other apps of the family/team and logout from one will logout from all other.'

  s.homepage         = 'https://bitbucket.org/agi_sso/ssocrossapp'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pankaj Verma' => 'pankaj.verma@timesinternet.in' }
  s.source           = { :git => 'https://bitbucket.org/agi_sso/ssocrossapp.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '7.0'

  s.source_files = 'CrossAppLogin/Classes/**/*'
  
  # s.resource_bundles = {
  #   'CrossAppLogin' => ['CrossAppLogin/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

s.frameworks = 'Accounts', 'Social', 'Foundation'
s.dependency 'FBSDKCoreKit'
s.dependency 'FBSDKLoginKit'
s.dependency 'Google/SignIn'
end
