# CrossAppLogin

[![CI Status](http://img.shields.io/travis/Pankaj Verma/CrossAppLogin.svg?style=flat)](https://travis-ci.org/Pankaj Verma/CrossAppLogin)
[![Version](https://img.shields.io/cocoapods/v/CrossAppLogin.svg?style=flat)](http://cocoapods.org/pods/CrossAppLogin)
[![License](https://img.shields.io/cocoapods/l/CrossAppLogin.svg?style=flat)](http://cocoapods.org/pods/CrossAppLogin)
[![Platform](https://img.shields.io/cocoapods/p/CrossAppLogin.svg?style=flat)](http://cocoapods.org/pods/CrossAppLogin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CrossAppLogin is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CrossAppLogin"
```

## Author

Pankaj Verma, pankaj.verma@timesinternet.in

## License

CrossAppLogin is available under the MIT license. See the LICENSE file for more info.
