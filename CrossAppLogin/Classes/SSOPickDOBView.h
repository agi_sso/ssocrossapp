//
//  SSOPickPhoto.h
//  Pods
//
//  Created by Pankaj Verma on 10/12/16.
//
//

#import <UIKit/UIKit.h>


@protocol  dobUpdatedDelegate<NSObject>
-(void)dobUpdated;
-(void)errorInUpdateDob:(NSString *)err_msg;
@end


@interface SSOPickDOBView : UIView
@property (nonatomic, weak) id <dobUpdatedDelegate> dobDelegate;

@property NSString *currentDob;
@property CGPoint ssoWebViewCenter;
@property CGRect ssoWebviewBounds;


@end
