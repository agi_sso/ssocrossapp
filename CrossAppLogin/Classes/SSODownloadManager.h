//
//  SSODownloadManager.h
//  Pods
//
//  Created by Pankaj Verma on 10/3/16.
//
//

#import <Foundation/Foundation.h>

@interface SSODownloadManager : NSObject{

}
+ (id)ssoSharedManager;
- (void)downloadDataForUrl:(NSString *)baseUrl path:(NSString *)path userInfo:(NSDictionary *)queryDictionary completionHandler:(void (^)(NSDictionary * _Nullable dataDictionary, NSString *err_msg))completionHandler;

@end
