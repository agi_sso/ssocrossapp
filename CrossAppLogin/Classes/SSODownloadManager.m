//
//  SSODownloadManager.m
//  Pods
//
//  Created by Pankaj Verma on 10/3/16.
//
//

#import "SSODownloadManager.h"
#import "SSOGlobal.h"
#import "SSOUrlManager.h"

@implementation SSODownloadManager

+ (id)ssoSharedManager {
    static SSODownloadManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (void)downloadDataForUrl:(NSString *)baseUrl path:(NSString *)path userInfo:(NSDictionary *)queryDictionary completionHandler:(void (^)(NSDictionary * _Nullable dataDictionary, NSString *err_msg))completionHandler
{
    NSMutableURLRequest *downloadRequest = [NSMutableURLRequest requestWithURL: [SSOUrlManager getUrlForUrl:baseUrl path:path andParameters:queryDictionary]];
    [downloadRequest setHTTPMethod:@"POST"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:downloadRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error == nil){
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
        completionHandler(json,NULL);
        }
        else{
            completionHandler(NULL,error.localizedDescription);
        }
    }];
                                  
    [task resume];
    
}
@end
