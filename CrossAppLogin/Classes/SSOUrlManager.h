//
//  SSOUrlManager.h
//  Pods
//
//  Created by Pankaj Verma on 10/5/16.
//
//

#import <Foundation/Foundation.h>

@interface SSOUrlManager : NSObject
+(NSURL *)getUrlForUrl:(NSString *)baseUrl path:(NSString *)path andParameters:(NSDictionary*)params;
+(NSString *)getScriptString:(NSDictionary*)params;
@end
