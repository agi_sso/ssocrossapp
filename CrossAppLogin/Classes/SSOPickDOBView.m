//
//  SSOPickPhoto.m
//  Pods
//
//  Created by Pankaj Verma on 10/12/16.
//
//

#import "SSOPickDOBView.h"
#import "SSODownloadManager.h"
#import "SSOGlobal.h"
#import "SSOKeychain.h"

@implementation SSOPickDOBView
@synthesize dobDelegate;

UIDatePicker * datePicker;

- (void)drawRect:(CGRect)rect {

    //tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(tappedOutSideDatePicker:)];
    
    tap.delegate = self;
    self.frame = _ssoWebviewBounds;
    self.center = _ssoWebViewCenter;//self.view.center
    self.backgroundColor = [UIColor blackColor];
    [self addGestureRecognizer:tap];
    
    //date picker
    datePicker = [[UIDatePicker alloc]init];
    
    datePicker.frame = CGRectMake(0.0,0.0, self.frame.size.width, 300);
    datePicker.backgroundColor = [UIColor lightGrayColor ];
    datePicker.layer.cornerRadius = 15.0;
    datePicker.center = self.ssoWebViewCenter;
    NSDateComponents * minusHundredYears = [NSDateComponents new];
    minusHundredYears.year = -100;
    NSDate *hundredYearsAgo = [[NSCalendar currentCalendar] dateByAddingComponents:minusHundredYears
                                                                            toDate:[NSDate new]
                                                                           options:0];
    datePicker.minimumDate = hundredYearsAgo;
    datePicker.maximumDate = [NSDate new];
    NSDate* returnedDate = [self getDate:_currentDob];
    [datePicker setDate: returnedDate animated:true];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    //done button
    UIButton * doneButton = [UIButton new];
    [doneButton setTitle:@"Update" forState:UIControlStateNormal];//setTitle("Update", forState: UIControlState.Normal)
    [doneButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
    doneButton.frame = CGRectMake(0,0,100,60);
    doneButton.center = CGPointMake(self.center.x, self.center.y-132);
    [doneButton addTarget:self action:@selector(dismissPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self addSubview:datePicker];
    [self addSubview:doneButton];
    
}
-(void) tappedOutSideDatePicker:(UITapGestureRecognizer*)sender {
    [self removeFromSuperview];
}

//MARK: UIGestureRecognizerDelegate
-(BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ( [touch.view isKindOfClass:[UIButton class]]) {
        [self dismissPicker:touch.view ];
        return false;
    }
    else{
        return true;
    }
    
}


-(void) dismissPicker:(UIButton *)sender {
    NSString *ticketId = [[SSOKeychain sharedKeychain] find:@"ticketId"];
    [[SSODownloadManager ssoSharedManager] downloadDataForUrl:SSOBaseUrl path:userBasicDataUrlPath userInfo:@{ @"ticketid":[ticketId length]==0?@"":ticketId,
                                                                                                               @"channel":[sso_channel length]==0?@"":sso_channel,
                                                                                                               @"siteid":[sso_siteid length]==0?@"":sso_siteid,
                                                                                                               @"dob":[self getDateString:[datePicker date]]}
    
            completionHandler:^(NSDictionary * _Nullable dataDictionary, NSString *err_msg)
     {
         if (err_msg == NULL)
         {
             [self.dobDelegate dobUpdated];
         }
         else{
             [self.dobDelegate errorInUpdateDob:err_msg];
         }
    }];
    [self removeFromSuperview];
}// end dismissPicker

// MARK - observer to get the change in date

-(NSDate *)getDate:(NSString*)date{
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"dd-MM-yyyy";//"dd/MM/yyyy"
    if (date != @"" && date != @"--" ){
        NSDate* d = [dateFormatter dateFromString:date];
        if(d==nil) return [NSDate new];
        else return d;
    }
    else{
        return [NSDate new];
    }
}

-(NSString *) getDateString:(NSDate *)date {
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    return [dateFormatter stringFromDate:date];
}


@end
