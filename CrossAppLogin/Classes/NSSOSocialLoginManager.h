//
//  NSSOSocialLoginManager.h
//  Pods
//
//  Created by Pankaj Verma on 09/11/16.
//
//

#import <UIKit/UIKit.h>
@protocol GoogleLoginDelegate <NSObject>

-(void) googleSignInSuccessfulWithInfo:(NSMutableDictionary *)info;
-(void) googleSignInFailWithError:(NSString *) error_message;

@end





@interface NSSOSocialLoginManager : NSObject
@property (nonatomic, weak) id <GoogleLoginDelegate> googleLoginDelegate;
+ (id) sharedManager;
-(void)loginViaFacebookOnViewController:(UIViewController *)vc completion:(void(^)(NSDictionary *info,NSString * error_message))completion;
-(void)loginViaGoogleOnViewController:(UIViewController *)vc ;
@end
