//
//  SSOGlobal.m
//  Pods
//
//  Created by Pankaj Verma on 10/4/16.
//
//

#import "SSOGlobal.h"

@implementation SSOGlobal
//SSO Urls
 NSString *const SSOBaseUrl = @"jsso.indiatimes.com";
 NSString *const SSOmSocialBaseUrl = @"socialappsintegrator.indiatimes.com";

 NSString *const loginUrlPath = @"/sso/app/identity/login/show";
 NSString *const profileUrlPath = @"/sso/app/identity/profile/edit";
 NSString *const loadingUrlPath = @"/sso/v1/mobile/jsp/loading.jsp";
 NSString *const getDataForDeviceUrlPath = @"/sso/app/getDataForDevice";
 NSString *const openChangeDPUrlPath =  @"/sso/app/identity/profile/pic";
 NSString *const updateImageUrlPath = @"/sso/app/AppsUpdateImageFile";
 NSString *const userBasicDataUrlPath = @"/sso/app/UpdateBasicUserData";

 NSString *const trapPageUrlPath = @"/msocialsite/app/trappagerequest";
 NSString *const signInWithGoogleResponse = @"/msocialsite/app/googleplusresponse";
 NSString *const signInWithFacebookResponse = @"/msocialsite/app/facebookresponse";

//SSO Callbacks
 NSString *const nativeFBLogin = @"nativeFBLogin";
 NSString *const nativeGPlusLogin = @"nativeGPlusLogin";
 NSString *const nativeDOBViewInvoke = @"nativedobviewinvoke";
 NSString *const nativeDPViewInvoke = @"nativedpviewinvoke";
 NSString *const returnFromLogout = @"returnFromLogout";
 NSString *const returnFromLogin = @"returnFromLogin";
 NSString *const openChangeDP =  @"openchangedp";
 NSString *const returnFromSsecLogin = @"returnFromSsecLogin";
 NSString *const nativeFBConnect = @"nativeFBConnect";
 NSString *const nativeGPlusConnect = @"nativeGPlusConnect";
 NSString *const returnFromSocialConnect = @"returnFromSocialConnect";
 NSString *const nativeFBConnectForDP = @"nativefbconnectfordp";
 NSString *const nativeGPlusConnectForDP = @"nativegplusconnectfordp";
 NSString *const returnFromTrapPage = @"returnFromTrapPage";
 NSString *const nativeSkip = @"nativeskip";
 NSString *const delinked = @"delinked";

// SSO Variables
 NSString * sso_siteid=@""; //"bb51be5a5a8a0283467e0859d262ff6f"
 NSString * sso_channel=@""; //"toicrossapp"//"timespoint"
 NSString * sso_teamId=@"";

//SSO Constants
 NSString *const secret = @"lksdkgehjabsxjnlabsxoqwunzxlznxwfsdhASJQYWTQWSHQGSNBASB";
 NSString *const devide_name =@"ios";
 NSString *const primaryEmailKey= @"PEML";
 BOOL  isSSOProfilePageOpen = false ;//should be var

//SSO Keys
 NSString *const sso_primaryEmail_key = @"PEML";
 NSString *const sso_ssec_key = @"ssec";
 NSString *const sso_ticketId_key = @"ticketId";
 NSString *const sso_oauthsiteid_key = @"oauthsiteid";
 NSString *const sso_tgId_key = @"tgId";

//SSO title header
 NSString * sso_titleString = @"Welcome";
 NSString * sso_HeaderBackgroundColorHexString =  @"be2037";

@end
