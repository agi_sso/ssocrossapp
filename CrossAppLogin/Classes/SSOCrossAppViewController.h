//
//  SSOCrossAppViewController.h
//  Pods
//
//  Created by Pankaj Verma on 10/3/16.
//
//

#import <UIKit/UIKit.h>
@protocol ssoLoginDelegate <NSObject>

-(void) loginSuccessful;
-(void) logoutSuccessful;
-(void) skippedLogin;

@end

@interface SSOCrossAppViewController : UIViewController{

}
@property (nonatomic, weak) id <ssoLoginDelegate> loginDelegate;


+(void) openSSOLoginPageOnViewController:(UIViewController *)vc delegate:(UIViewController *)delObj;

+(id) sharedLoginController;


+(void)getUserDetails:(void (^)(NSDictionary * _Nullable dataDictionary))completion;
+(void)ssoSetupForChannel:(NSString *)channel siteId:(NSString *)siteid teamId:(NSString *)teamId isPassiveLoginEnabled:(BOOL)passiveLoginEnabled;//1.0.4
+(void)setSSoHeaderTitle:(NSString *)title andBackgroungColorWithHexString:(NSString *)hex;//1.0.5
-(NSString *)getTicketId;//1.0.5
@end
