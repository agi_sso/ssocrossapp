//
//  NSSOSocialLoginManager.m
//  Pods
//
//  Created by Pankaj Verma on 09/11/16.
//
//

#import "NSSOSocialLoginManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
@interface NSSOSocialLoginManager ()<GIDSignInDelegate,GIDSignInUIDelegate>

@end

@implementation NSSOSocialLoginManager
@synthesize googleLoginDelegate;
static NSSOSocialLoginManager *singletonObject = nil;

+ (id) sharedManager
{
    if (! singletonObject) {
        
        singletonObject = [[NSSOSocialLoginManager alloc] init];
        [FBSDKAppEvents activateApp];
        [GIDSignIn sharedInstance].delegate = self;
    }
    return singletonObject;
}

- (id)init
{
    if (! singletonObject) {
        
        singletonObject = [super init];
        [FBSDKAppEvents activateApp];
        [GIDSignIn sharedInstance].delegate = self;
    }
    return singletonObject;
}


//Facebook
-(void)loginViaFacebookOnViewController:(UIViewController *)vc completion:(void(^)(NSDictionary *info,NSString * error_message))completion{
    
//    if ([FBSDKAccessToken currentAccessToken]){
//        NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
//        [info setObject: FBSDKAccessToken.currentAccessToken.userID forKey:@"oauthId"];
//        [info setObject: FBSDKAccessToken.currentAccessToken.tokenString forKey:@"accessToken"];
//    
//        completion(info,NULL);
//        [[FBSDKLoginManager alloc] logOut];
//        return;
//    }
    FBSDKLoginManager *facebookLoginManager = [[FBSDKLoginManager alloc] init];
    [facebookLoginManager logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:vc handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if(error != nil || [result isCancelled]){
            [facebookLoginManager logOut];
            completion(NULL,error.localizedDescription);
        }
        else{
            NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
            [info setObject: result.token.userID forKey:@"oauthId"];
            [info setObject: result.token.tokenString forKey:@"accessToken"];
            completion(info,NULL);
        }
    }];
    [[FBSDKLoginManager alloc] logOut];

}
//Google
-(void)loginViaGoogleOnViewController:(UIViewController *)vc{
    [GIDSignIn sharedInstance].uiDelegate = vc;
    [GIDSignIn sharedInstance].delegate = self;
    [[GIDSignIn sharedInstance] signIn];
//    if ([GIDSignIn sharedInstance].currentUser){
//        [self getGoogleDataForUser:[GIDSignIn sharedInstance].currentUser];
//    }else{
//        [[GIDSignIn sharedInstance] signIn];
//    }
}
-(void) getGoogleDataForUser:(GIDGoogleUser *)user{
    
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
    [info setObject: user.userID forKey:@"oauthId"];
    [info setObject: user.authentication.accessToken forKey:@"accessToken"];
    [[GIDSignIn sharedInstance] signOut];
    [self.googleLoginDelegate googleSignInSuccessfulWithInfo:info];
}

#pragma mark GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
    if(error){
        [self.googleLoginDelegate googleSignInFailWithError:error.localizedDescription];
    }
    else{
        [self getGoogleDataForUser:user];
    }
}
@end
