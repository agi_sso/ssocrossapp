//
//  SSOCrossAppViewController.m
//  Pods
//
//  Created by Pankaj Verma on 10/3/16.
//
//

#import "SSOCrossAppViewController.h"
#import "SSOKeychain.h"
#import "SSOGlobal.h"
#import "SSODownloadManager.h"
#import "SSOUrlManager.h"
#import "SSOPickDOBView.h"
#import "NSSOSocialLoginManager.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed: ((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green: ((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue: ((float)(rgbValue & 0xFF))/255.0 alpha: 1.0]


@interface SSOCrossAppViewController ()<UIWebViewDelegate,UIImagePickerControllerDelegate,dobUpdatedDelegate,UIActionSheetDelegate,GoogleLoginDelegate>

-(void) showSSOLoginPage;
-(void) showSSOProfilePage;
-(void) showSSOTrapPage: (NSString *)stateid;
-(void) showSSOLoadingPage;
-(void) showSSODPPage;
-(void) userLogout;
-(void) setSsecLogin: (NSString *)email;
-(void) saveTsec: (NSString *)oauthsiteid
            ssec: (NSString *)ssec
        ticketId: (NSString *)ticketId
           email: (NSString *)email;

-(void) updateTsec: (NSString *)oauthsiteid
              ssec: (NSString *)ssec
          ticketId: (NSString *)ticketId
             email: (NSString *)email;

-(void) removeTsec;

-(void)dataFromSocialNetwork: (NSString *)oauthSiteId
                      userId: (NSString *)userId
                 accessToken: (NSString *)accessToken;

-(void) loginToFacebook;
-(void) signInGoogle;

@end

@implementation SSOCrossAppViewController
@synthesize loginDelegate;

UIWebView * ssoWebview;
UIView * ssoActivityIndicatorView;
UIActivityIndicatorView * ssoActivityIndicator;
UIView *titleHeaderView;
CGFloat titleHeaderViewHeight = 48;
bool isCrossAppLoginViewControllerComesFromBackground = false;
NSString * ssoSocialConnectType = @"";

NSString *local_ssec = @"";
NSString *local_ticketId = @"";
NSString *local_email = @"";
NSString *local_oauthsiteid = @"";

bool isPassiveLogin = false;

static SSOCrossAppViewController *singletonObject = nil;

+ (id) sharedLoginController
{
    if (! singletonObject)
    {
        singletonObject = [[SSOCrossAppViewController alloc] init];
    }
    return singletonObject;
}

- (id)init
{
    if (! singletonObject)
    {
        singletonObject = [super init];
        // Uncomment the following line to see how many times is the init method of the class is called
        // NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return singletonObject;
}

//Stop auto rotate
- (BOOL)shouldAutorotate
{
    if ([self isKindOfClass: [SSOCrossAppViewController class]])
    {
        return NO;
    }
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.tintColor = [UIColor whiteColor];
    
    // the webviwe
    [self instantiateTheWebView];
    
    //the back button
    [self instantiateTheTitleHeaderView];
    
    //the activity indicator
    [self instatiateTheActivityIndicaotor];
    
    //will be called one time when logi button pressed as the class is Singleton
    [self refreshSSOLogin];
    
    //get notified when app comes to foreground
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(willEnterForeground:)
                                                 name: UIApplicationWillEnterForegroundNotification
                                               object: nil];
}

-(void)instantiateTheWebView
{
    //pull to refresh
    UIRefreshControl * refreshControl = [UIRefreshControl new];
    [refreshControl addTarget: self action: @selector(pullToRefresh:) forControlEvents: UIControlEventValueChanged];
    ssoWebview = [[UIWebView alloc] initWithFrame: CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y+titleHeaderViewHeight, self.view.bounds.size.width, self.view.bounds.size.height-titleHeaderViewHeight)];
    [ssoWebview.scrollView addSubview: refreshControl];
    ssoWebview.dataDetectorTypes = UIDataDetectorTypeNone;
    ssoWebview.delegate = self;
    [self.view addSubview: ssoWebview];
}

-(void)instantiateTheTitleHeaderView
{
    titleHeaderView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, titleHeaderViewHeight)];//NSLayoutAttributeTop
    unsigned colorInt = 0;
    [[NSScanner scannerWithString: sso_HeaderBackgroundColorHexString] scanHexInt: &colorInt];
    titleHeaderView.backgroundColor = UIColorFromRGB(colorInt);;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 17.0, 40.0, 30.0)];    [backButton addTarget: self
                                                                                                               action: @selector(goBackWithBackButton:)
                                                                                                     forControlEvents: UIControlEventTouchUpInside];
    UIImage *backIcon = [UIImage imageNamed: @"ssoBack"];
    if (backIcon == NULL)
    {
        [backButton setTitle: @"Back" forState: UIControlStateNormal];
    }
    else
    {
        [backButton setImage: backIcon forState: UIControlStateNormal];
        //[backButton setBackgroundImage: backIcon forState: UIControlStateNormal];
    }
    [backButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [titleHeaderView addSubview: backButton];
    
    // title label
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(titleHeaderViewHeight, 21, self.view.bounds.size.width-titleHeaderViewHeight*2, 21)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    label.lineBreakMode = UILineBreakModeWordWrap;
    label.text = sso_titleString;
    [titleHeaderView addSubview: label];
    
    [self.view sendSubviewToBack: titleHeaderView];
    [self.view addSubview: titleHeaderView];
}

-(void)instatiateTheActivityIndicaotor
{
    ssoActivityIndicatorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 100, 100)];
    ssoActivityIndicatorView.layer.cornerRadius = 10;
    ssoActivityIndicatorView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2,ssoWebview.center.y);
    ssoActivityIndicatorView.backgroundColor = [UIColor darkGrayColor];
    ssoActivityIndicatorView.alpha = 0.5;
    
    // ActivityIndicator
    ssoActivityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    ssoActivityIndicator.center = CGPointMake(ssoActivityIndicatorView.frame.size.width / 2,                                              ssoActivityIndicatorView.frame.size.height / 2);
    ssoActivityIndicator.hidesWhenStopped = true;
    [ssoActivityIndicatorView addSubview: ssoActivityIndicator];
}

-(void) willEnterForeground: (NSNotification *)notification
{
    isCrossAppLoginViewControllerComesFromBackground = true;
    [self refreshSSOLogin];
}

-(void) goBackWithBackButton: (UIButton*)sender
{
    if (ssoWebview.canGoBack)
    {
        NSURL * currentUrl = [[ssoWebview request] URL];
        NSURL * profileUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                                    path: profileUrlPath
                                           andParameters: @{@"channel": [sso_channel length]==0?@"": sso_channel,@"device":  @"ios"}];
        NSURL * loginUrl =  [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                                   path: loginUrlPath
                                          andParameters: @{@"channel": [sso_channel length]==0?@"": sso_channel,@"device":  @"ios"}];
        
        if ([currentUrl isEqual: loginUrl]||[currentUrl isEqual: profileUrl])
        {
            // don't go back
            [self closeCrossAppLoginViewController];
            
        }
        else
        {
            [ssoWebview goBack];
        }
    } // can't go back
    else
    {
        [self closeCrossAppLoginViewController];
    }
}

-(void) pullToRefresh: (UIRefreshControl *)refreshCtrl
{
    [ssoWebview reload];
    [refreshCtrl endRefreshing];
}



+(void)gettgid: (BOOL)passiveLoginEnabled
{
    if([[[SSOKeychain sharedKeychain] find: sso_tgId_key] length] == 0)
    {
        NSString *deviceid = [UIDevice currentDevice].identifierForVendor.UUIDString;
        NSDictionary* info = @{
                               @"channel": [sso_channel length]==0?@"": sso_channel,
                               @"deviceid": [deviceid length]==0?@"": deviceid
                               };
        
        [[SSODownloadManager ssoSharedManager] downloadDataForUrl: SSOBaseUrl
                                                             path: getDataForDeviceUrlPath
                                                         userInfo: info
                                                completionHandler: ^(NSDictionary * _Nullable dataDictionary,NSString *err_msg)
         {
             if (err_msg==NULL)
             {
                 [[SSOKeychain sharedKeychain] insert: sso_tgId_key:  [[dataDictionary valueForKey: sso_tgId_key] dataUsingEncoding: NSUTF8StringEncoding]];
                 if(passiveLoginEnabled)
                 {
                     [self openPassiveLoginPage];
                 }
             }
             else
             {
                 NSLog(@"%@", err_msg);
             }
         }];
    }
    else
    {
        NSLog(@"tgId found");//App is already installed
    }
}

+(void)openPassiveLoginPage
{
    isPassiveLogin = true;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[self sharedLoginController] loginToFacebook];
    });
    
}

-(void)showSSOLoginPage
{
    //  isSSOProfilePageOpen = false;
    NSURL *loginUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                             path: loginUrlPath
                                    andParameters: @{@"channel":  [sso_channel length]==0?@"": sso_channel,@"device":  @"ios"}];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL: loginUrl
                                              cachePolicy: NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: 10.0];
    [ssoWebview loadRequest: req];
}

-(void) showSSOProfilePage
{
    NSURL *profileUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                               path: profileUrlPath
                                      andParameters: @{@"channel":  [sso_channel length]==0?@"": sso_channel,@"device":  @"ios"}];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL: profileUrl
                                              cachePolicy: NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: 10.0];
    [ssoWebview loadRequest: req];
}

-(void) showSSOTrapPage: (NSString *)stateid
{
    NSURL *trapPageUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                                path: trapPageUrlPath
                                       andParameters: @{@"channel":  [sso_channel length]==0?@"": sso_channel,@"device":  @"ios",@"stateid":  stateid}];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL: trapPageUrl
                                              cachePolicy: NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: 10.0];
    [ssoWebview loadRequest: req];
}

-(void) showSSOLoadingPage
{
    // on complete loading of the page setSsecLogin("") will be called
    NSURL *loadingUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                               path: loadingUrlPath
                                      andParameters: nil];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL: loadingUrl
                                              cachePolicy: NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: 10.0];
    [ssoWebview loadRequest: req];
}

-(void) showSSODPPage
{
    NSURL *openChangeDPUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl
                                                    path: openChangeDPUrlPath
                                           andParameters: @{@"channel":  [sso_channel length]==0?@"": sso_channel,@"device":  @"ios"}];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL: openChangeDPUrl
                                              cachePolicy: NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: 10.0];
    [ssoWebview loadRequest: req];
}

-(void) userLogout
{
    NSString * script =  [NSString stringWithFormat: @"userLogout('%@','%@')", sso_channel,devide_name];
    NSString * result =  [ssoWebview stringByEvaluatingJavaScriptFromString: script];
    NSLog(@"%@", result);
    NSLog(@"returnFromLogout will be invoked after success");
}

-(void) setSsecLogin: (NSString *)email
{
    NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    NSString * oauthsiteid = [[SSOKeychain sharedKeychain] find: sso_oauthsiteid_key ];
    NSString * tgId = [[SSOKeychain sharedKeychain] find: sso_tgId_key];
    NSString * script =  [NSString stringWithFormat: @"setSsecLogin('%@','%@','%@','%@','%@','%@','%@')", sso_channel,ssec,secret,tgId, devide_name,oauthsiteid,email];
    
    NSString * result = [ssoWebview stringByEvaluatingJavaScriptFromString: script];
    NSLog(@"%@", result);
    NSLog(@"returnFromSsecLogin will be invoked on successed");
}


-(void) saveTsec: (NSString *)oauthsiteid
            ssec: (NSString *)ssec
        ticketId: (NSString *)ticketId
           email: (NSString *)email
{
    //local
    local_ssec = ssec;
    local_ticketId = ticketId;
    local_email = email;
    local_oauthsiteid = oauthsiteid;
    
    //shared
    [[SSOKeychain sharedKeychain] insert: sso_oauthsiteid_key:  [oauthsiteid dataUsingEncoding: NSUTF8StringEncoding]];
    [[SSOKeychain sharedKeychain] insert: sso_ssec_key:  [ssec dataUsingEncoding: NSUTF8StringEncoding]];
    [[SSOKeychain sharedKeychain] insert: sso_ticketId_key:  [ticketId dataUsingEncoding: NSUTF8StringEncoding]];
    if([email length]!=0)
    {
        [[SSOKeychain sharedKeychain] insert: sso_primaryEmail_key:  [email dataUsingEncoding: NSUTF8StringEncoding]];
    }
}

-(void) removeTsec
{
    //local
    local_ssec = @"";
    local_ticketId = @"";
    local_email = @"";
    local_oauthsiteid = @"";
    
    //shared
    [[SSOKeychain sharedKeychain] remove: sso_oauthsiteid_key];
    [[SSOKeychain sharedKeychain] remove: sso_ssec_key];
    [[SSOKeychain sharedKeychain] remove: sso_ticketId_key];
    [[SSOKeychain sharedKeychain] remove: sso_primaryEmail_key];
    
}

-(void) updateTsec: (NSString *)oauthsiteid
              ssec: (NSString *)ssec
          ticketId: (NSString *)ticketId
             email: (NSString *)email
{
    //local
    local_ssec = ssec;
    local_ticketId = ticketId;
    local_email = email;
    local_oauthsiteid = oauthsiteid;
    
    //shared
    [[SSOKeychain sharedKeychain] update: sso_oauthsiteid_key:  [oauthsiteid dataUsingEncoding: NSUTF8StringEncoding]];
    [[SSOKeychain sharedKeychain] update: sso_ssec_key:  [ssec dataUsingEncoding: NSUTF8StringEncoding]];
    [[SSOKeychain sharedKeychain] update: sso_ticketId_key:  [ticketId dataUsingEncoding: NSUTF8StringEncoding]];
    if([email length]!=0)
    {
        [[SSOKeychain sharedKeychain] update: sso_primaryEmail_key:  [email dataUsingEncoding: NSUTF8StringEncoding]];
    }
}



-(void)copySharedDataToLocal
{
    local_ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    local_ticketId = [[SSOKeychain sharedKeychain] find: sso_ticketId_key];
    local_email = [[SSOKeychain sharedKeychain] find: sso_primaryEmail_key];
    local_oauthsiteid = [[SSOKeychain sharedKeychain] find: sso_oauthsiteid_key];
}

//class public methods
+(void)ssoSetupForChannel: (NSString *)channel
                   siteId: (NSString *)siteid
                   teamId: (NSString *)teamId
    isPassiveLoginEnabled: (BOOL)passiveLoginEnabled
{
    sso_channel = channel;
    sso_siteid = siteid;
    sso_teamId = teamId;
    [self gettgid: passiveLoginEnabled];
}

+(NSDictionary *)isLoginToSSO
{
    NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    NSString * ticketId = [[SSOKeychain sharedKeychain] find: sso_ticketId_key];
    NSNumber *isLogin = @NO;
    if([ssec length]!=0) isLogin = @YES;
    if ([ssec length] == 0)
    {
        ssec = @"";
    }
    if ([ticketId length] == 0)
    {
        ticketId = @"";
    }
    NSDictionary * tsecInfo = @{
                                sso_ssec_key:  [ssec length]==0?@"": ssec,
                                sso_ticketId_key:  [ticketId length]==0?@"": ticketId,
                                @"isLogin":  isLogin
                                };
    return tsecInfo;
}


+(void)getUserDetails: (void (^)(NSDictionary * _Nullable dataDictionary))completion
{
    NSString *ssoNativeApiPath = @"/sso/crossapp/identity/native/";
    NSString *path = @"getUserDetails";
    NSString *host = SSOBaseUrl;
    NSString *fullPath = [NSString stringWithFormat: @"%@%@",ssoNativeApiPath,path];
    
    NSURLComponents *components = [[NSURLComponents alloc] init];
    components.scheme = @"https";
    components.host = host;
    components.path = fullPath;
    NSURL * url = components.URL;
    
    NSMutableURLRequest *downloadRequest = [NSMutableURLRequest requestWithURL: url];
    [downloadRequest setHTTPMethod: @"POST"];
    
    [downloadRequest setValue: @"application/json" forHTTPHeaderField: @"Accept"];
    [downloadRequest setValue: @"application/json" forHTTPHeaderField: @"Content-Type"];
    
    NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    NSString * ticketId = [[SSOKeychain sharedKeychain] find: sso_ticketId_key];
    NSString * tgid = [[SSOKeychain sharedKeychain] find: sso_tgId_key];
    
    NSString *const sso_tgid_key = @"tgid";
    NSString *const sso_channel_key = @"channel";
    NSString *const sso_appVersion_key = @"appVersion";
    NSString *const sso_platform_key = @"platform";
    NSString *const sso_ssec_key = @"ssec";
    NSString *const sso_ticketId_key = @"ticketId";
    
    NSDictionary *sso_header_params = @{
                                        sso_tgid_key: [tgid length]==0?@"": tgid,
                                        sso_ssec_key: [ssec length]==0?@"": ssec,
                                        sso_ticketId_key: [ticketId length]==0?@"": ticketId,
                                        sso_channel_key: [sso_channel length]==0?@"": sso_channel,
                                        sso_appVersion_key: @"1.0.0",
                                        sso_platform_key: @"ios"
                                        };
    
    
    for (NSString *key in sso_header_params)
    {
        [downloadRequest setValue: sso_header_params[key] forHTTPHeaderField: key];
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest: downloadRequest
                                            completionHandler: ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                  {
                                      if (error == nil)
                                      {
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData: data options: 0 error: nil];
                                          NSLog(@"%@", json);
                                          completion(json);
                                      }
                                      else
                                      {
                                          NSLog(error.localizedDescription);
                                      }
                                  }];
    
    [task resume];
}


-(void) refreshSSOLogin
{
    //check internet
    NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    
    if (!isCrossAppLoginViewControllerComesFromBackground)
    { //first time loaded
        if([ssec length]==0)
        {
            [self showSSOLoginPage];
        }
        else
        {
            [self copySharedDataToLocal];
            [self showSSOLoadingPage];
        }
    }
    else
    {//ComesFromBackground
        if([ssec length]==0 && !isSSOProfilePageOpen || ([ssec length]!=0 && isSSOProfilePageOpen && [local_ssec isEqualToString: ssec]))
        {
            //do nothing
        }
        else
        {
            [self showSSOLoadingPage];
        }
    }
}

+(void) openSSOLoginPageOnViewController: (UIViewController *)vc delegate: (UIViewController *)delObj
{
    [vc.navigationController setNavigationBarHidden: YES animated: YES];
    SSOCrossAppViewController *slvc =  [[SSOCrossAppViewController alloc] init];
    slvc.loginDelegate = delObj;
    [vc presentViewController: slvc animated: true completion: nil];
    
}

//-(void) closeCrossAppLoginViewController {
//     [self.navigationController setNavigationBarHidden: NO animated: YES];
//     //[titleHeaderView removeFromSuperview];
//    [self dismissViewControllerAnimated: true completion: nil];
//}

-(void) closeCrossAppLoginViewController
{
    if ([self.loginDelegate isKindOfClass: [UIViewController class]])
    {
        UIViewController *vc = (UIViewController *)self.loginDelegate;
        [vc.navigationController setNavigationBarHidden: NO animated: YES];
    }
    else
    {
        [self.navigationController setNavigationBarHidden: NO animated: YES];
    }
    
    [self dismissViewControllerAnimated: true completion: nil];
}

//Mark: set sso header color and title
+(void)setSSoHeaderTitle: (NSString *)title andBackgroungColorWithHexString: (NSString *)hex
{
    if(title != nil && title != @"")
    {
        sso_titleString = title;
    }
    if(hex != nil && hex != @"")
    {
        sso_HeaderBackgroundColorHexString = hex;
    }
}

//MARK: spining indicator
-(void) ssoSpiningIndicatorShow
{
    [self.view addSubview: ssoActivityIndicatorView];
    [ssoActivityIndicator startAnimating];
}

-(void) ssoSpiningIndicatorHide
{
    [ssoActivityIndicator startAnimating];
    [ssoActivityIndicatorView removeFromSuperview];
}


//MARK: - SSO login functions

-(void) doChangesAfterLogout
{
    isSSOProfilePageOpen = false;
    [self removeTsec];
    [self showSSOLoginPage];
    [self closeCrossAppLoginViewController];
    [self.loginDelegate logoutSuccessful];
}

-(void) doChangesAfterLogin
{
    isSSOProfilePageOpen = true;
    [self showSSOProfilePage];
   // NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    if(!isCrossAppLoginViewControllerComesFromBackground)
    {
        isCrossAppLoginViewControllerComesFromBackground = true;
        return;
    }
    else
    {
    [self closeCrossAppLoginViewController];
    if(isPassiveLogin)
    {
        isPassiveLogin = false;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
        [self showAlertWithTitle: @"Alert!" alertMessage: @"Automatically Loged in with your Facebook"];
    }
    [self.loginDelegate loginSuccessful];
    }
}

-(void) doChangesAfterSkip
{
    isSSOProfilePageOpen = false;
    [self closeCrossAppLoginViewController];
    [self.loginDelegate skippedLogin];
}

//    func getSucceccfullResponse(message: String) -> Bool;


-(void)pickDateOfBirth: (NSString *)dob
{
    SSOPickDOBView *pickDOBView = [[SSOPickDOBView alloc] initWithFrame: CGRectMake(0.0,0.0, self.view.frame.size.width, 300)];
    pickDOBView.dobDelegate = self;
    pickDOBView.currentDob = dob;
    pickDOBView.ssoWebviewBounds = ssoWebview.bounds;
    pickDOBView.ssoWebViewCenter = ssoWebview.center;
    [self.view addSubview: pickDOBView];
}

#pragma mark dobUpdatedDelegate method
-(void)dobUpdated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [ssoWebview reload];
    });
}

-(void)errorInUpdateDob: (NSString *)err_msg
{
    NSLog(@"%@",err_msg);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAlertWithTitle: @"Error!" alertMessage: err_msg];
    });
}

#pragma mark UIWebViewDelegate
-(BOOL)webView: (UIWebView *)webView shouldStartLoadWithRequest: (NSURLRequest *)request navigationType: (UIWebViewNavigationType)navigationType
{
    NSURL * url = request.URL;
    NSString * str = [[[url absoluteString] componentsSeparatedByString: [NSString stringWithFormat: @"%@://",url.scheme]] lastObject];
    NSString * msg = [str stringByRemovingPercentEncoding];
    
    SWITCH ([url scheme])
    {
        CASE (nativeFBLogin)
        {
            ssoSocialConnectType = nativeFBLogin;
            [self loginToFacebook];
            break;
        }
        CASE (nativeGPlusLogin)
        {
            ssoSocialConnectType = nativeGPlusLogin;
            [self signInGoogle] ;
            break;
        }
        CASE (nativeDOBViewInvoke)
        {
            [self pickDateOfBirth: msg];
            break;
        }
        CASE (nativeDPViewInvoke)
        {
            [self pickPhoto];
            break;
        }
        CASE (returnFromLogout)
        {
            [self doChangesAfterLogout ];
            break;
        }
        CASE (returnFromSsecLogin)
        { //if self.getSucceccfullResponse(message){
            [self ssoSpiningIndicatorHide];
            [self doChangesAfterLogin];
            break;
        }
        CASE (returnFromLogin)
        {
            [self gettsecFromMobileLogin: msg];
            break;
        }
        CASE (openChangeDP)
        {
            [self showSSODPPage];
            break;
        }
        CASE (returnFromSocialConnect)
        {
            [ssoWebview reload ];
            break;
        }
        CASE (nativeFBConnect)
        {   //check below 4 connect ssoSocialConnectTypes
            ssoSocialConnectType = nativeFBConnect;
            [self loginToFacebook ];
            break;
        }
        CASE (nativeGPlusConnect)
        {
            ssoSocialConnectType = nativeGPlusConnect;
            [self signInGoogle];
            break;
        }
        CASE (nativeFBConnectForDP)
        {
            ssoSocialConnectType = nativeFBConnectForDP;
            [self loginToFacebook];
            break;
        }
        CASE (nativeGPlusConnectForDP)
        {
            ssoSocialConnectType = nativeGPlusConnectForDP;
            [self signInGoogle ];
            break;
        }
        CASE (returnFromTrapPage)
        {
            [self gettsecFromTrapPage: msg ];
            break;
        }
        CASE (nativeSkip)
        {
            [self doChangesAfterSkip ];
            break;
        }
        CASE (delinked)
        {
            [ssoWebview reload ];
            break;
        }
        DEFAULT
        {
            // NSLog(@"don't care url scheme");
            break;
        }
    }//end of SWITCH
    return true;
}

-(void)webViewDidStartLoad: (UIWebView *)webView
{
    [self ssoSpiningIndicatorShow ];
    // [self.view sendSubviewToBack: titleHeaderView];//weak self
}

-(void)webViewDidFinishLoad: (UIWebView *)webView
{
    [self ssoSpiningIndicatorHide];
    
    //[ssoWebview bringSubviewToFront: titleHeaderView];
    
    [ssoWebview stringByEvaluatingJavaScriptFromString: @"document.body.style.webkitTouchCallout='none';"];
    
    NSURL * currentUrl = [[webView request] URL];
    NSURL * loadingUrl = [SSOUrlManager getUrlForUrl: SSOBaseUrl path: loadingUrlPath andParameters: nil];
    
    if ([currentUrl isEqual: loadingUrl])
    {
        NSString * ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
        if ([ssec length]==0)
        {
            [self userLogout];//  silent logout --> Login button
        }
        else
        {
            [self setSsecLogin: [[SSOKeychain sharedKeychain] find: sso_primaryEmail_key ]];
        }
    }
}

-(void)webView: (UIWebView *)webView didFailLoadWithError: (NSError *)error
{
    NSLog([error localizedDescription]);
    [self ssoSpiningIndicatorHide];
    // [ssoWebview bringSubviewToFront: titleHeaderView];
    if([error.localizedDescription isEqualToString: @"The URL can’t be shown"])
    {
        //it's ok
    }
    else if([error.localizedDescription rangeOfString: @"The operation"].location != NSNotFound)
    {
        //it's ok
    }
    else
        [self showAlertWithTitle: @"" alertMessage: error.localizedDescription];
}

#pragma mark response handler methods


-(void) gettsecFromTrapPage: (NSString *)message
{
    NSData * data = [message dataUsingEncoding: NSUTF8StringEncoding];
    NSDictionary * dataDict  = [NSJSONSerialization JSONObjectWithData: data options: 0 error: nil];
    NSString * ssec =  [dataDict valueForKey: sso_ssec_key];
    NSString * ticketId = [dataDict valueForKey: sso_ticketId_key];
    if([ssec length]!=0 && [ticketId length]!=0)
    {
        [self saveTsec: @"facebook" ssec: ssec ticketId: ticketId email: @""];
        [self doChangesAfterLogin];
    }
}


-(void) gettsecFromMobileLogin: (NSString *)message
{
    NSData * data = [message dataUsingEncoding: NSUTF8StringEncoding];
    NSDictionary * dataDict  = [NSJSONSerialization JSONObjectWithData: data options: 0 error: nil];
    NSString * ssec =  [dataDict valueForKey: sso_ssec_key];
    NSString * ticketId = [dataDict valueForKey: sso_ticketId_key];
    NSString * email = [dataDict valueForKey: @"email"];
    
    if([ssec length]!=0 && [ticketId length]!=0)
    {
        [self saveTsec: @"sso" ssec: ssec ticketId: ticketId email: email];
        [self setSsecLogin: email];
    }
    
}

//MARK: - Upload profile pic
-(void)pickPhoto
{
    if ([UIAlertController class])
    {//iOS 8.0 and above
        UIAlertController * alert = [UIAlertController alertControllerWithTitle: @"UPLOAD PROFILE PICTURE" message: @"" preferredStyle: UIAlertControllerStyleAlert];
        [alert addAction: [UIAlertAction actionWithTitle: @"Photo Gallery    "
                                                  style: UIAlertActionStyleDefault
                                                handler: ^(UIAlertAction * _Nonnull action)
                          {
                              //MARK: imagePickerController
                              UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                              picker.delegate = self;
                              picker.allowsEditing = true;
                              picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                              //self->view.window!.rootViewController!
                              
                              [self presentViewController: picker animated: true completion: ^{}];
                              
                          }]
         ];
        
        [alert addAction: [UIAlertAction actionWithTitle: @"Camera    "
                                                  style: UIAlertActionStyleDefault
                                                handler: ^(UIAlertAction * _Nonnull action)
                          {
                              //MARK: imagePickerController
                              UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                              picker.delegate = self;
                              picker.allowsEditing = true;
                              picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                              [self presentViewController: picker animated: true completion: ^{}]; //self.view.window.rootViewController
                          }]
         ];
        
        //present image picker
        [self presentViewController: alert
                           animated: true
                         completion: ^{
                             alert.view.superview.userInteractionEnabled = true;
                             [alert.view.superview addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(alertControllerBackgroundTapped:)]];
                         }];
    }
    else
    { //Below iOS 8.0
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: @"UPLOAD PROFILE PICTURE"
                                                                 delegate: self cancelButtonTitle: @"Cancel"
                                                   destructiveButtonTitle: nil
                                                        otherButtonTitles: @"Photo Gallery    ", @"Camera    ", nil];
        actionSheet.tag = 232;
        [actionSheet showInView: self.view];
    }
}

-(void)actionSheet: (UIActionSheet *)actionSheet didDismissWithButtonIndex: (NSInteger)buttonIndex
{
    if (actionSheet.tag == 232)
    {
        if (![[NSString class] respondsToSelector: @selector(containsString)])
        {//ios 7
            if([[actionSheet buttonTitleAtIndex: buttonIndex] rangeOfString: @"Photo Gallery"].location != NSNotFound)
            {
                //Photo Library
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
                
            }
            else if([[actionSheet buttonTitleAtIndex: buttonIndex] rangeOfString: @"Camera"].location != NSNotFound)
            {
                //Camera
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
                
            }
        }
        else  //for iOS 8
        {
            if([[actionSheet buttonTitleAtIndex: buttonIndex] containsString: @"Photo Gallery"])
            {
                //Photo Library
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
            }
            else if([[actionSheet buttonTitleAtIndex: buttonIndex] containsString: @"Camera"])
            {
                //Camera
                UIImagePickerController * picker = [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.allowsEditing = true;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self.view.window.rootViewController presentViewController: picker animated: true completion: ^{}];
            }
        }
    }
}



-(void) alertControllerBackgroundTapped: (UITapGestureRecognizer *)tap
{
    [self dismissViewControllerAnimated: true completion: nil];
}

#pragma mark UIImagePickerDelegate
- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    UIImage * editedImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated: true completion: ^{
        [self uploadRequest: editedImage];
    }];
}


//MARK: UploadRequest
-(NSString *) generateBoundaryString
{
    return [NSString stringWithFormat: @"Boundary-%@",[[NSUUID UUID] UUIDString]];
}

-(void) uploadRequest: (UIImage *)imageToSend
{
    NSData * image_data = UIImagePNGRepresentation(imageToSend);
    //NSData * image_dataJpeg = UIImageJPEGRepresentation(imageToSend,0.8);
    
    
    NSUInteger imageSize = image_data.length;
    NSUInteger imageSizeInKb = imageSize/1024;
    NSLog(@"size of image in KB: %lu", (unsigned long)imageSizeInKb);
    if (imageSizeInKb > 2048)
    {
        [self showAlertWithTitle: @"Error!"
                    alertMessage: [NSString stringWithFormat: @"Image size is %lu KB which is larger than we accept (2048 KB). Crop it to appropriate size and upload.",(unsigned long)imageSizeInKb]];
        
        return;
    }
    
    NSString * ticketId = [[SSOKeychain sharedKeychain] find: sso_ticketId_key];
    NSString * boundary = [self generateBoundaryString];
    
    NSString * formBoundary = [NSString stringWithFormat: @"--%@\r\nContent-Disposition: form-data; name=",boundary];
    
//    NSDictionary * param = @{
//                             @"Content-Type":  [NSString stringWithFormat: @"multipart/form-data; boundary=%@",boundary]
//                             };
    
    NSString * ticketId_body = [NSString stringWithFormat: @"%@\"ticketid\"\r\n\r\n%@\r\n",formBoundary,ticketId];
    NSString * siteid_body = [NSString stringWithFormat: @"%@\"siteid\"\r\n\r\n%@\r\n",formBoundary,sso_siteid];
    NSString * img_body = [NSString stringWithFormat: @"%@\"file\";filename=\"test.png\"\r\nContent-Type: image/png\r\n\r\n",formBoundary];
    NSString * tail_body = [NSString stringWithFormat: @"\r\n--%@--\r\n",boundary];
    //print or nslog ticketId_body,siteid_body and img_body to check if they are in correct format
    NSMutableData * body_data = [[NSMutableData alloc]init];
    [body_data appendData: [ticketId_body dataUsingEncoding: NSUTF8StringEncoding]];
    [body_data appendData: [siteid_body dataUsingEncoding: NSUTF8StringEncoding]];
    [body_data appendData: [img_body dataUsingEncoding: NSUTF8StringEncoding]];
    [body_data appendData: image_data];
    [body_data appendData: [tail_body dataUsingEncoding: NSUTF8StringEncoding]];
    
    
    NSURL * url = [NSURL URLWithString: [NSString stringWithFormat:  @"https://%@%@",SSOBaseUrl,updateImageUrlPath]];//
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL: url];
    
    [request setHTTPMethod: @"POST"];
    [request setValue: [NSString stringWithFormat: @"multipart/form-data; boundary=%@",boundary] forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody: body_data];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest: request
                                            completionHandler: ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                  {
                                      if (error == nil)
                                      {
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData: data options: 0 error: nil];
                                          NSLog(@"%@", json);
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
                                              [ssoWebview reload];
                                          });
                                          
                                      }
                                      else
                                      {
                                          NSLog(@"%@", error.localizedDescription);
                                          [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
                                      }
                                  }];
    
    [task resume];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
}



//MARK: - Social login and connect

-(void) loginToFacebook
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
    
    // [[NSSOSocialLoginManager sharedManager] loginViaFacebookOnViewController: [[[UIApplication sharedApplication] keyWindow] rootViewController] completion: ^(NSDictionary *info, NSString *error_message) {
    UIViewController *presentingVC = self;
    if(isPassiveLogin)
    {
        presentingVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    }
    [[NSSOSocialLoginManager sharedManager] loginViaFacebookOnViewController: presentingVC
                                                                  completion: ^(NSDictionary *info, NSString *error_message)
     {
         if(error_message == NULL)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 ssoSocialConnectType = nativeFBLogin;
                 if(!isPassiveLogin)
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
                 [self dataFromSocialNetwork: @"facebook"
                                      userId: [info valueForKey: @"oauthId"]
                                 accessToken: [info valueForKey: @"accessToken"]];
             });
         }
         else
         {//failure in Facebook Login
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
                 [self showAlertWithTitle: @"Alert!" alertMessage: error_message];
             });
         }
     }];
    
}

-(void) signInGoogle
{
    NSSOSocialLoginManager * loginManager = [[NSSOSocialLoginManager alloc] init];
    loginManager.googleLoginDelegate = self;
    
    [loginManager loginViaGoogleOnViewController: self];
}

#pragma After Google sign in
-(void) googleSignInSuccessfulWithInfo: (NSMutableDictionary *)info
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
    [self dataFromSocialNetwork: @"googleplus" userId: [info valueForKey: @"oauthId"] accessToken: [info valueForKey: @"accessToken"]];
}

-(void) googleSignInFailWithError: (NSString *) error_message
{//failure in Google Sign in
    [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
    [self showAlertWithTitle: @"Alert!" alertMessage: error_message];//check for main thread
}


-(void)dataFromSocialNetwork: (NSString *)oauthSiteId
                      userId: (NSString *)userId
                 accessToken: (NSString *)accessToken
{
    NSString *urlPath = @"";
    [self ssoSpiningIndicatorShow];
    SWITCH(ssoSocialConnectType)
    {
        CASE(nativeGPlusConnect)
        {
            [self setSocialConnect: oauthSiteId userId: userId accessToken: accessToken];
            return;
        }
        CASE(nativeFBConnect)
        {
            [self setSocialConnect: oauthSiteId userId: userId accessToken: accessToken];
            return;
        }
        CASE(nativeFBConnectForDP)
        {
            [self setSocialConnect: oauthSiteId userId: userId accessToken: accessToken];
            return;
        }
        CASE(nativeGPlusConnectForDP)
        {
            [self setSocialConnect: oauthSiteId userId: userId accessToken: accessToken];
            return;
        }
        
        CASE(nativeGPlusLogin)
        {
            urlPath = signInWithGoogleResponse;
            break;
        }
        CASE(nativeFBLogin)
        {
            urlPath = signInWithFacebookResponse;
            break;
        }
        DEFAULT
        {
            return;
        }
    }//end of SWITCH
    //Login using social info
    // handle null insertion exception
    NSDictionary* params = @{@"oauthId": [userId length]==0?@"": userId,
                             @"siteId": [sso_siteid length]==0?@"": sso_siteid,
                             @"accessToken": [accessToken length]==0?@"": accessToken,
                             @"deviceId": [UIDevice currentDevice].identifierForVendor.UUIDString,
                             @"ssecreq": @"yes",
                             @"channel": [sso_channel length]==0?@"": sso_channel,
                             @"sitereg": [sso_channel length]==0?@"": sso_channel
                             };
    
    [[SSODownloadManager ssoSharedManager] downloadDataForUrl: SSOmSocialBaseUrl
                                                         path: urlPath
                                                     userInfo: params
                                            completionHandler: ^(NSDictionary * _Nullable dataDictionary, NSString * err_msg)
     {
         if(err_msg == NULL)
         {
             NSString * ssec =  [dataDictionary valueForKey: sso_ssec_key];
             NSString * ticketId = [dataDictionary valueForKey: sso_ticketId_key];
             if([ssec length]!=0 && [ticketId length]!=0)
             {
                 [self saveTsec: oauthSiteId ssec: ssec ticketId: ticketId email: @""];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self setSsecLogin: @""];
                 });
             }
             else
             {
                 [self ssoSpiningIndicatorHide];
                 if ([[dataDictionary valueForKey: @"code"] intValue] == 497)
                 {
                     [self showSSOTrapPage: [dataDictionary valueForKey: @"stateid"]];
                 }
                 if ([[dataDictionary valueForKey: @"code"] intValue] == 498)
                 {
                     NSLog(@"Looks you are using old token");
                 }
             }
         }//error
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if(isPassiveLogin)
                 {
                     isPassiveLogin = false;
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
                 }
                 [self ssoSpiningIndicatorHide];
                 [self showAlertWithTitle: @"" alertMessage: err_msg];
             });
             
         }
     }];
}

-(void)setSocialConnect: (NSString *)oauthSiteId
                 userId: (NSString *)userId
            accessToken: (NSString *)accessToken
{
    NSString *ssec = [[SSOKeychain sharedKeychain] find: sso_ssec_key];
    NSString * script =  [NSString stringWithFormat: @"setSocialConnect('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", userId,oauthSiteId,accessToken,ssec,sso_siteid,@"ios",@"",@"fname",@"lname",@"bday",@"M",@"htown",@"location",@"NO",@"imgpath"];
    [ssoWebview stringByEvaluatingJavaScriptFromString: script];
    NSLog(@"Connecting:  returnFromSocialConnect messageHandler will be invoked when successed");
}

#pragma Show alert
-(void)showAlertWithTitle: (NSString *)title
             alertMessage: (NSString *)alert_msg
{
    [[[UIAlertView alloc] initWithTitle: @""
                                message: alert_msg
                               delegate: self
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil, nil]
     show];
}

#pragma get ticket
-(NSString *)getTicketId
{
    NSString *ticketId = [[SSOKeychain sharedKeychain] find: sso_ticketId_key];
    if([ticketId length]==0)
    {
        ticketId = @"";
    }
    return ticketId;
}
@end
