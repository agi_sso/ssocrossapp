//
//  SSOUrlManager.m
//  Pods
//
//  Created by Pankaj Verma on 10/5/16.
//
//

#import "SSOUrlManager.h"
#import "SSOGlobal.h"

@implementation SSOUrlManager
+(NSURL *)getUrlForUrl:(NSString *)baseUrl path:(NSString *)path andParameters:(NSDictionary*)params{
NSURLComponents *components = [[NSURLComponents alloc] init];
    components.scheme = @"https";
    components.host = baseUrl;
    components.path = path;

    //iOS 8.0 and above
   // NSMutableArray *queryItems = [NSMutableArray array];
//    for (NSString *key in params) {
//        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:params[key]]];
//    }
//    components.queryItems = queryItems;
    
    NSMutableString *queries = [[NSMutableString alloc] init];
    for (NSString *key in params) {
        [queries appendString:[NSString stringWithFormat:@"%@=%@&",key,params[key]]];
    }
    if ([queries length] > 0) {
        queries = [queries substringToIndex:[queries length] - 1];
    }
    components.query = queries;
    return components.URL;
}

@end
