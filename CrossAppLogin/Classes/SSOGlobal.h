//
//  SSOGlobal.h
//  Pods
//
//  Created by Pankaj Verma on 10/4/16.
//
//

#import <Foundation/Foundation.h>

#define CASE(str)                       if( [__s__ caseInsensitiveCompare:(str)] == NSOrderedSame)
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT


@interface SSOGlobal : NSObject
//SSO Base Urls
extern NSString *const SSOBaseUrl;
extern NSString *const SSOmSocialBaseUrl;

//url paths
extern NSString *const loginUrlPath;
extern NSString *const profileUrlPath;
extern NSString *const loadingUrlPath;
extern NSString *const getDataForDeviceUrlPath;
extern NSString *const signInWithGoogleResponse;
extern NSString *const signInWithFacebookResponse;
extern NSString *const openChangeDPUrlPath;
extern NSString *const updateImageUrlPath;
extern NSString *const userBasicDataUrlPath;
extern NSString *const trapPageUrlPath;

//SSO Callbacks
extern NSString *const nativeFBLogin ;
extern NSString *const nativeGPlusLogin ;
extern NSString *const nativeDOBViewInvoke ;
extern NSString *const nativeDPViewInvoke ;
extern NSString *const returnFromLogout ;
extern NSString *const returnFromLogin ;
extern NSString *const openChangeDP;
extern NSString *const returnFromSsecLogin ;
extern NSString *const nativeFBConnect ;
extern NSString *const nativeGPlusConnect ;
extern NSString *const returnFromSocialConnect ;
extern NSString *const nativeFBConnectForDP ;
extern NSString *const nativeGPlusConnectForDP ;
extern NSString *const returnFromTrapPage ;
extern NSString *const nativeSkip ;
extern NSString *const delinked ;

// SSO Variables
extern NSString * sso_siteid; //"bb51be5a5a8a0283467e0859d262ff6f"
extern NSString * sso_channel; //"toicrossapp"//"timespoint"
extern NSString * sso_teamId;

//SSO Constants
extern NSString *const secret ;//= "lksdkgehjabsxjnlabsxoqwunzxlznxwfsdhASJQYWTQWSHQGSNBASB"
extern NSString *const devide_name;// = "ios"
extern NSString *const primaryEmailKey;// = "PEML"
extern BOOL isSSOProfilePageOpen;

//SSO Keys
extern NSString *const sso_primaryEmail_key;
extern NSString *const sso_ssec_key;
extern NSString *const sso_ticketId_key;
extern NSString *const sso_oauthsiteid_key;
extern NSString *const sso_tgId_key;


//SSO title header
//#be2037
extern NSString * sso_titleString;
extern NSString * sso_HeaderBackgroundColorHexString;

@end
