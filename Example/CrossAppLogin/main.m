//
//  main.m
//  CrossAppLogin
//
//  Created by Pankaj Verma on 10/03/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

@import UIKit;
#import "SSOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SSOAppDelegate class]));
    }
}
