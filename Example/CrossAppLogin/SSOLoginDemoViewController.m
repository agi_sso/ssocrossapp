//
//  SSOLoginDemoViewController.m
//  CrossAppLogin
//
//  Created by Pankaj Verma on 28/11/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "SSOLoginDemoViewController.h"
#import "SSOCrossAppViewController.h"
@interface SSOLoginDemoViewController ()<ssoLoginDelegate>

@end

@implementation SSOLoginDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [SSOCrossAppViewController openSSOLoginPageOnViewController:self delegate:self];

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:true];
//    [SSOCrossAppViewController openSSOLoginPageOnViewController:self delegate:self];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getTicket:(id)sender {
    NSString *t = [[SSOCrossAppViewController sharedLoginController] getTicketId];

    [[SSOCrossAppViewController sharedLoginController] getTicketId];
    [[[UIAlertView alloc] initWithTitle:@"TicketId" message:t delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (IBAction)getUserDetail:(id)sender {
    [SSOCrossAppViewController  getUserDetails :^(NSDictionary * _Nullable dataDictionary) {
        NSLog(@"%@", dataDictionary);
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:[NSString stringWithFormat:@"User details: \n %@", dataDictionary] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        });
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark ssoLoginDelegate methods
-(void)loginSuccessful{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"loginSuccessful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    
}
-(void)logoutSuccessful{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"logoutSuccessful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    
}
-(void)skippedLogin{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"skippedLogin" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    
}

@end
