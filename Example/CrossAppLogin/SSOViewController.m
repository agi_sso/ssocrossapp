//
//  SSOViewController.m
//  CrossAppLogin
//
//  Created by Pankaj Verma on 10/03/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

#import "SSOViewController.h"
#import "SSOCrossAppViewController.h"
#import "SSOLoginDemoViewController.h"

@interface SSOViewController ()<ssoLoginDelegate>

@end

@implementation SSOViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginToSSO:(id)sender {
    //1
//    SSOCrossAppViewController *sso_calvc ;
//    sso_calvc =  [[SSOCrossAppViewController alloc] init];
//    sso_calvc.loginDelegate = self;
//    [sso_calvc openSSOLoginPage:sso_calvc onViewController: self];
    
    //2
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SSOLoginDemoViewController *demoViewController = [storyboard instantiateViewControllerWithIdentifier:@"SSOLoginDemoViewController"];
    [self.navigationController pushViewController:demoViewController animated:true];
    //[self presentViewController:demoViewController animated:true completion:nil];
    
    
    //3
  // [SSOCrossAppViewController openSSOLoginPageOnViewController:self delegate:self];
}


- (IBAction)openProfile:(id)sender {
    [SSOCrossAppViewController openSSOLoginPageOnViewController:self delegate:self];
}


#pragma mark ssoLoginDelegate methods
-(void)loginSuccessful{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"loginSuccessful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

}
-(void)logoutSuccessful{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"logoutSuccessful" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

}
-(void)skippedLogin{
    [[[UIAlertView alloc] initWithTitle:@"" message:@"skippedLogin" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

}
@end
