//
//  SSOAppDelegate.h
//  CrossAppLogin
//
//  Created by Pankaj Verma on 10/03/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

@import UIKit;

@interface SSOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
