//
//  SSOAppDelegate.m
//  CrossAppLogin
//
//  Created by Pankaj Verma on 10/03/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

#import "SSOAppDelegate.h"
#import "SSOCrossAppViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
@implementation SSOAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [GIDSignIn sharedInstance].clientID = @"35393984251-lo2k8ligk8uv85m035p720lbum5l2ahg.apps.googleusercontent.com";

   [SSOCrossAppViewController ssoSetupForChannel:@"toicrossapp" siteId:@"bb51be5a5a8a0283467e0859d262ff6f" teamId:@"RB3CUQ8JTM" isPassiveLoginEnabled:TRUE];
    [SSOCrossAppViewController setSSoHeaderTitle:@"Times Recipe" andBackgroungColorWithHexString:@"be2037"];//6 hex digit
  //  [SSOCrossAppViewController setSSoHeaderTitle:NULL andBackgroungColorWithHexString:NULL];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL canHandleFBURL = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    BOOL canHandleGIDURL = [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
    BOOL canHandleURL = canHandleFBURL || canHandleGIDURL;
    return canHandleURL;
}

@end
